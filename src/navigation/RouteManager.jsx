import { Route, Redirect } from "react-router-dom";
import { Keys } from "../util/Keys";
export { Route, Switch, BrowserRouter, Redirect } from "react-router-dom";

export const AuthenticRoute = (props) => {
  let redirectPath = "/login";
  if (localStorage.getItem(Keys.ACCESS_TOKEN) === null) {
    const renderComponent = () => <Redirect to={{ pathname: redirectPath }} />;
    return <Route {...props} component={renderComponent} render={undefined} />;
  } else {
    return <Route {...props} />;
  }
};
