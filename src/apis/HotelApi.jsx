import { Api, AuthenticApi } from "./ApiManager";

export const HotelApi = {
  getAllHotels: () => {
    return Api.get("/hotels");
  },

  getHotelById: (id) => {
    return Api.get(`/hotels/${id}`);
  },

  createHotel: (data) => {
    return AuthenticApi.post("/hotels", JSON.stringify(data));
  },

  updateHotel: (data) => {
    return AuthenticApi.put(`/hotels/${data.id}`, JSON.stringify(data));
  },

  deleteHotel: (id) => {
    return AuthenticApi.delete(`/hotels/${id}`);
  },
};
