import { Api } from "./ApiManager";

export const AuthApi = {
  login: (data) => {
    return Api.post("auth/login", JSON.stringify(data));
  },
};
