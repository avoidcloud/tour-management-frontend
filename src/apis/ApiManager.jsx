import axios from "axios";
import { Keys } from "../util/Keys";

const LoginPaths = ["/login"];
const API_SERVER = "http://localhost:9000";

function isUserAlreadyInLoginPath() {
  let pathname = window.location.pathname
    ? window.location.pathname.toLowerCase()
    : "";

  return LoginPaths.findIndex((p) => p === pathname) >= 0;
}

function handleSuccessfulRequest(response) {
  return response;
}

async function handleFailedRequest(error) {
  if (error.response) {
    let status = error.response.status;
    if (status === 401) {
      if (!isUserAlreadyInLoginPath()) {
        localStorage.removeItem(Keys.ACCESS_TOKEN);
        window.location.replace("/login");
      }
    }
  }
  return Promise.reject(error);
}

export function WithInterceptors(axios) {
  axios.interceptors.response.use(handleSuccessfulRequest, handleFailedRequest);
  return axios;
}

export const Api = WithInterceptors(
  axios.create({
    baseURL: API_SERVER,
    headers: { "Content-Type": "application/json" },
  })
);

export const AuthenticApi = WithInterceptors(
  axios.create({
    baseURL: API_SERVER,
    headers: {
      Authorization: `Bearer ${localStorage.getItem(Keys.ACCESS_TOKEN)}`,
    },
  })
);

export const AuthenticatedAPPAPI = WithInterceptors(
  axios.create({
    baseURL: API_SERVER,
    headers: { "Proxy-Authorization": process.env.REACT_APP_API_KEY },
  })
);
