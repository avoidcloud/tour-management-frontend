import { Api, AuthenticApi } from "./ApiManager";

export const PlaceApi = {
  getAllPlaces: () => {
    return Api.get("/places");
  },

  getPlaceById: (id) => {
    return Api.get(`/places/${id}`);
  },

  createPlace: (data) => {
    return AuthenticApi.post("/places", JSON.stringify(data));
  },

  updatePlace: (data) => {
    return AuthenticApi.put(`/places/${data.id}`, JSON.stringify(data));
  },

  deletePlace: (id) => {
    return AuthenticApi.delete(`/places/${id}`);
  },
};
