import { AuthenticApi } from "./ApiManager";

export const ProfileApi = {
  getProfile: () => {
    return AuthenticApi.get("/profile");
  },
};
