import React from "react";
import { Layout } from "../../components/Layout";
import { HotelApi } from "../../apis/HotelApi";
import { HotelCard } from "./HotelCard";

export const HotelsContainer = () => {
  const [hotels, setHotels] = React.useState([]);

  const loadHotels = async () => {
    try {
      let res = await HotelApi.getAllHotels();
      setHotels(res.data);
    } catch (err) {
      alert(err);
    }
  };

  React.useEffect(() => {
    const componentDidMount = () => {
      loadHotels();
    };

    componentDidMount();
  }, []);

  return (
    <Layout>
      <div className="mt-4 mb-4">
        <h2 className="text-center">Book Your Suit</h2>
      </div>
      {hotels.map((item, index) => (
        <HotelCard hotel={item} key={index} />
      ))}
    </Layout>
  );
};
