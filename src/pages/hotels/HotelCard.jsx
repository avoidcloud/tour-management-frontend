import { Col, Container, Image, ListGroupItem, Row } from "react-bootstrap";

export const HotelCard = (props) => {
  return (
    <ListGroupItem>
      <Container fluid>
        <Row>
          <Col sm={4}>
            <Image
              width="300px"
              height="300px"
              fluid
              rounded
              src={props.hotel.imageUrl}
            />
          </Col>
          <Col sm={8}>
            <h3>
              <a href={`/hotels/${props.hotel._id}`}>{props.hotel.name}</a>
            </h3>
            <h5 className="text-secondary">Location: {props.hotel.address}</h5>
            <h5 className="text-success">
              Booking Price: {props.hotel.bookingPrice}
            </h5>
            <p>{props.hotel.description}</p>
          </Col>
        </Row>
      </Container>
    </ListGroupItem>
  );
};
