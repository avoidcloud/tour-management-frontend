import React from "react";
import { PlaceApi } from "../../apis/PlaceApi";
import { Layout } from "../../components/Layout";
import { PlaceView } from "./PlaceView";
import { RelatedPlaces } from "./RelatedPlaces";

export const PlaceDetailContainer = (props) => {
  const id = props.match.params.id;

  const [place, setPlace] = React.useState();
  const [places, setPlaces] = React.useState([]);

  const loadPlaceDetail = async (placeId) => {
    try {
      let res = await PlaceApi.getPlaceById(placeId);
      setPlace(res.data);
    } catch (err) {
      alert(err);
    }
  };

  const loadPlaces = async () => {
    try {
      let res = await PlaceApi.getAllPlaces();
      setPlaces(res.data);
    } catch (err) {
      alert(err);
    }
  };

  React.useEffect(() => {
    const componentDidMount = () => {
      loadPlaceDetail(id);
      loadPlaces();
    };

    componentDidMount();
  });

  if (!place) {
    return (
      <Layout>
        <div className="mt-5">
          <h6 className="text-center">Loading...</h6>
        </div>
      </Layout>
    );
  }

  return (
    <Layout>
      <PlaceView place={place} />
      <RelatedPlaces places={places} />
    </Layout>
  );
};
