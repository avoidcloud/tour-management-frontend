export const RelatedPlaces = (props) => {
  const { places } = props;
  return (
    <div className="mt-5">
      <h4 className="text-center">Related Places</h4>
      <div className="container-fluid">
        <div className="row">
          {places.map((place, index) => (
            <div className="col-sm-3" key={index}>
              <div className="card border-primary mb-3">
                <div className="card-header">{place.address}</div>
                <div className="card-body text-primary">
                  <h5 className="card-title">
                    <a href={`/places/${place._id}`}>{place.name}</a>
                  </h5>
                  <img
                    className="card-img-top"
                    src={place.imageUrl}
                    height="150px"
                    alt={place.name}
                  />
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};
