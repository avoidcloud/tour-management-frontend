import React from "react";
import { Layout } from "../../components/Layout";
import { ProfileApi } from "../../apis/ProfileApi";
import { Col, ListGroupItem, Row, Spinner } from "react-bootstrap";

export const ProfileContainer = () => {
  const [profile, setProfile] = React.useState();

  const loadProfile = async () => {
    try {
      let res = await ProfileApi.getProfile();
      setProfile(res.data);
    } catch (err) {
      alert(err);
    }
  };

  React.useEffect(() => {
    const componentDidMount = () => {
      loadProfile();
    };
    componentDidMount();
  }, []);

  return (
    <Layout>
      <div className="mt-5">
        <h5 className="text-center">My Profile</h5>
        {profile ? (
          <Row className="d-flex justify-content-center">
            <Col md={4}>
              <ListGroupItem>Name : {profile.name}</ListGroupItem>
              <ListGroupItem>Email : {profile.email}</ListGroupItem>
              <ListGroupItem>Gender : {profile.gender}</ListGroupItem>
            </Col>
          </Row>
        ) : (
          <div className="text-center">
            <Spinner />
          </div>
        )}
      </div>
    </Layout>
  );
};
