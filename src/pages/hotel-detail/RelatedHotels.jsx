export const RelatedHotels = (props) => {
  const { hotels } = props;
  return (
    <div className="mt-5">
      <h4 className="text-center">Related Hotels</h4>
      <div className="container-fluid">
        <div className="row">
          {hotels.map((hotel, index) => (
            <div className="col-sm-3" key={index}>
              <div className="card border-primary mb-3">
                <div className="card-header">{hotel.address}</div>
                <div className="card-body text-primary">
                  <h5 className="card-title">
                    <a href={`/hotels/${hotel._id}`}>{hotel.name}</a>
                  </h5>
                  <img
                    className="card-img-top"
                    src={hotel.imageUrl}
                    height="150px"
                    alt={hotel.name}
                  />
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};
