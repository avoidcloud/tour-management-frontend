import { Image } from "react-bootstrap";

export const HotelView = (props) => {
  const { hotel } = props;

  return (
    <div>
      <div className="container-fluid">
        <div className="row d-flex justify-content-center">
          <div className="col-sm-5">
            <Image width="100%" src={hotel.imageUrl} />
          </div>
          <div className="col-sm-4">
            <div>
              <h4 className="text-center mt-2 mb-2">{hotel.name}</h4>
              <p>{hotel.description}</p>
            </div>
          </div>
        </div>
      </div>

      <div className="d-flex justify-content-center">
        <div className="col-sm-8">
          {hotel.features.map((item, index) => (
            <p key={index} className="list-group-item">
              {item}
            </p>
          ))}
        </div>
      </div>

      <hr />

      <div className="container-fluid">
        <h4 className="text-center">Photo Gallery</h4>
        <div className="row justify-content-center">
          {hotel.allImages.map((item, index) => (
            <div className="col-sm-4 m-4" key={index}>
              <Image height="300px" width="100%" src={item} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};
