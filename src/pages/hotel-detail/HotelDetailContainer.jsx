import React from "react";
import { Layout } from "../../components/Layout";
import { HotelApi } from "../../apis/HotelApi";
import { HotelView } from "./HotelView";
import { RelatedHotels } from "./RelatedHotels";

export const HotelDetailContainer = (props) => {
  const id = props.match.params.id;

  const [hotel, setHotel] = React.useState();
  const [hotels, setHotels] = React.useState([]);

  const loadHotelDetail = async (hotelId) => {
    try {
      let res = await HotelApi.getHotelById(hotelId);
      setHotel(res.data);
    } catch (err) {
      alert(err);
    }
  };

  const loadHotels = async () => {
    try {
      let res = await HotelApi.getAllHotels();
      setHotels(res.data);
    } catch (err) {
      alert(err);
    }
  };

  React.useEffect(() => {
    const componentDidMount = () => {
      loadHotelDetail(id);
      loadHotels();
    };

    componentDidMount();
  });

  if (!hotel) {
    return (
      <Layout>
        <div className="mt-5">
          <h6 className="text-center">Loading...</h6>
        </div>
      </Layout>
    );
  }

  return (
    <Layout>
      <HotelView hotel={hotel} />
      <RelatedHotels hotels={hotels} />
    </Layout>
  );
};
