import React from "react";
import { Layout } from "../../components/Layout";
import { PlaceApi } from "../../apis/PlaceApi";
import { Container, Row } from "react-bootstrap";
import { PlaceCard } from "./PlaceCard";

export const PlacesContainer = () => {
  const [places, setPlaces] = React.useState([]);

  const loadPlaces = async () => {
    try {
      let res = await PlaceApi.getAllPlaces();
      setPlaces(res.data);
    } catch (err) {
      alert(err);
    }
  };

  React.useEffect(() => {
    const componentDidMount = () => {
      loadPlaces();
    };
    componentDidMount();
  }, []);

  return (
    <Layout>
      <div className="mt-4 mb-4">
        <h2 className="text-center">Explore Destination Places</h2>
      </div>
      <Container fluid>
        <Row>
          {places.map((item, index) => (
            <PlaceCard place={item} key={index} />
          ))}
        </Row>
      </Container>
    </Layout>
  );
};
