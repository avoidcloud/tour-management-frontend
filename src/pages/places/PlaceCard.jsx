import { Col, Container, Image, ListGroupItem, Row } from "react-bootstrap";

export const PlaceCard = (props) => {
  return (
    <ListGroupItem>
      <Container fluid>
        <Row>
          <Col sm={4}>
            <Image
              width="300px"
              height="300px"
              fluid
              rounded
              src={props.place.imageUrl}
            />
          </Col>
          <Col sm={8}>
            <h3>
              {" "}
              <a href={`/places/${props.place._id}`}>{props.place.name}</a>{" "}
            </h3>
            <h5 className="text-secondary">Location: {props.place.address}</h5>

            <p>{props.place.description}</p>
          </Col>
        </Row>
      </Container>
    </ListGroupItem>
  );
};
