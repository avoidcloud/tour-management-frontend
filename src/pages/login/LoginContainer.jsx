import { Col, Container, Form, Row, Button } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { Layout } from "../../components/Layout";
import { AuthApi } from "../../apis/AuthApi";
import { Keys } from "../../util/Keys";

export const LoginContainer = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    handleLogin(data);
  };

  const handleLogin = async (data) => {
    try {
      let res = await AuthApi.login(data);
      localStorage.setItem(Keys.ACCESS_TOKEN, res.data.bearer);
      window.location.replace("/profile");
    } catch (err) {
      alert(err);
    }
  };

  return (
    <Layout>
      <Container>
        <Row className="d-flex justify-content-center">
          <Col md={4} sm={6}>
            <div className="list-group-item mt-5">
              <h6 className="text-center mt-2 mb-2">Login</h6>
              <Form onSubmit={handleSubmit(onSubmit)}>
                <Form.Group className="m-2">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    {...register("email", { required: true })}
                    placeholder="admin@admin.com"
                    type="email"
                    name="email"
                    inputMode="email"
                  />
                  {errors.email && (
                    <small className="text-danger">email is required</small>
                  )}
                </Form.Group>
                <Form.Group className="m-2">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    {...register("password", { required: true })}
                    placeholder="Password"
                    type="password"
                    name="password"
                  />
                  {errors.password && (
                    <small className="text-danger">password is required</small>
                  )}
                </Form.Group>

                <Button type="submit" className="m-2">
                  Login
                </Button>
              </Form>
            </div>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
};
