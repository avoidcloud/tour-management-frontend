import { Layout } from "../../components/Layout";

export const AdminContainer = () => {
  return (
    <Layout>
      <h1>Admin Panel</h1>
    </Layout>
  );
};
