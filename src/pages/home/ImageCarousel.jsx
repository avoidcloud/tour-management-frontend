import { Carousel } from "react-bootstrap";

export const ImageCarousel = () => {
  return (
    <Carousel variant="dark">
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="/images/world-tour.webp"
          alt="First slide"
          height="400px"
        />
        <Carousel.Caption
          style={{
            backgroundColor: "rgba(255,255,255,0.5)",
            borderRadius: "2rem",
          }}
        >
          <h3>World Tour</h3>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="/images/family-tour.jpeg"
          alt="Second slide"
          height="400px"
        />

        <Carousel.Caption
          style={{
            backgroundColor: "rgba(255,255,255,0.5)",
            borderRadius: "2rem",
          }}
        >
          <h3>Family Tour</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="/images/water-fall.jpeg"
          alt="Third slide"
          height="400px"
        />

        <Carousel.Caption
          style={{
            backgroundColor: "rgba(255,255,255,0.5)",
            borderRadius: "2rem",
          }}
        >
          <h3>Friends Tour</h3>
          <p>
            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
};
