import { Container, Row, Col, Card, Button } from "react-bootstrap";

export const HomeMenu = () => {
  return (
    <Container fluid>
      <Row>
        <Col md={6} sm={12}>
          <Card className="text-center">
            <Card.Img variant="top" src="images/family-tour.jpeg" />
            <Card.Body>
              <Card.Title>Special title treatment</Card.Title>
              <Card.Text>
                With supporting text below as a natural lead-in to additional
                content.
              </Card.Text>
              <a href="/hotels">
                <Button variant="primary">Go somewhere</Button>
              </a>
            </Card.Body>
          </Card>
        </Col>
        <Col md={6} sm={12}>
          <Card className="text-center">
            <Card.Img variant="top" src="images/family-tour.jpeg" />
            <Card.Body>
              <Card.Title>Special title treatment</Card.Title>
              <Card.Text>
                With supporting text below as a natural lead-in to additional
                content.
              </Card.Text>
              <a href="/places">
                <Button variant="primary">Go somewhere</Button>
              </a>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
