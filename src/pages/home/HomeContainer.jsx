import { Container } from "react-bootstrap";
import { Layout } from "../../components/Layout";
import { HomeMenu } from "./HomeMenu";
import { ImageCarousel } from "./ImageCarousel";

export const HomeContainer = () => {
  return (
    <Layout>
      <Container>
        <ImageCarousel />
        <div
          style={{
            marginTop: "8rem",
            marginBottom: "1rem",
          }}
        >
          <h1 className="text-center">Explore Your Destination</h1>
        </div>
        <HomeMenu />
      </Container>
    </Layout>
  );
};
