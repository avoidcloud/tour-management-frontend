import React from "react";

export const LogoutContainer = () => {
  React.useEffect(() => {
    const componentDidMount = () => {
      localStorage.clear();
      window.location.replace("/login");
    };
    componentDidMount();
  }, []);
  return <></>;
};
