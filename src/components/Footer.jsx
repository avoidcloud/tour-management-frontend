import { Col, Row, Container } from "react-bootstrap";

export const Footer = () => {
  return (
    <div className="bg-dark">
      <Container>
        <Row>
          <Col md={4} sm={12}>
            <ul className="text-white p-5">
              <li>Item 1</li>
              <li>Item 2</li>
              <li>Item 3</li>
              <li>Item 4</li>
            </ul>
          </Col>
          <Col md={4} sm={12}>
            <ul className="text-white p-5">
              <li>Item 1</li>
              <li>Item 2</li>
              <li>Item 3</li>
              <li>Item 4</li>
            </ul>
          </Col>
          <Col md={4} sm={12}>
            <ul className="text-white p-5">
              <li>Item 1</li>
              <li>Item 2</li>
              <li>Item 3</li>
              <li>Item 4</li>
            </ul>
          </Col>
        </Row>
      </Container>
    </div>
  );
};
