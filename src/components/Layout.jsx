import { Container, Nav, Navbar } from "react-bootstrap";
import { Keys } from "../util/Keys";
import { Footer } from "./Footer";

export const Layout = (props) => {
  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="/">Le Tour</Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link href="/places">Places</Nav.Link>
            <Nav.Link href="/hotels">Hotels</Nav.Link>
            {localStorage.getItem(Keys.ACCESS_TOKEN) ? (
              <>
                <Nav.Link href="/profile">Profile</Nav.Link>
                <Nav.Link href="/logout">Logout</Nav.Link>
              </>
            ) : (
              <Nav.Link href="/login">Login</Nav.Link>
            )}
          </Nav>
        </Container>
      </Navbar>
      {props.children}
      <div style={{ marginTop: "5rem" }}>
        <Footer />
      </div>
    </>
  );
};
