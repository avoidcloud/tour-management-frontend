import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { HomeContainer } from "./pages/home/HomeContainer";
import { PlacesContainer } from "./pages/places/PlacesContainer";
import { HotelsContainer } from "./pages/hotels/HotelsContainer";
import { LoginContainer } from "./pages/login/LoginContainer";
import { ProfileContainer } from "./pages/profile/ProfileContainer";
import { AuthenticRoute } from "./navigation/RouteManager";
import { LogoutContainer } from "./pages/logout/LogoutContainer";
import { AdminContainer } from "./pages/admin/AdminContainer";
import { PlaceDetailContainer } from "./pages/place-detail/PlaceDetailContainer";
import { HotelDetailContainer } from "./pages/hotel-detail/HotelDetailContainer";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={HomeContainer} />
        <Route path="/places" exact component={PlacesContainer} />
        <Route path="/places/:id" exact component={PlaceDetailContainer} />
        <Route path="/hotels" exact component={HotelsContainer} />
        <Route path="/hotels/:id" exact component={HotelDetailContainer} />
        <Route path="/login" exact component={LoginContainer} />
        <AuthenticRoute path="/profile" exact component={ProfileContainer} />
        <AuthenticRoute path="/logout" exact component={LogoutContainer} />
        <AuthenticRoute path="/admin" exact component={AdminContainer} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
